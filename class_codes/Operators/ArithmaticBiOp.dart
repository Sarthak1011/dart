main(){

  int x = 10;
  int y = 20;

  print(x + y);//30
  print(x - y);//-10
  print(x * y);//200
  print(x / y);//0.5
  print(x % y);//10
  print(x ~/ y);//0
}