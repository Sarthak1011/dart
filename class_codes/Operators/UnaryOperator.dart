void main(){

  int x = 10;

  print(x++);//10
  print(++x);//12
  print(x);//12

  int ans = ++x + ++x + x++;
  print(ans);//41
}