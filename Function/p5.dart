class Student {
  String name = "Rajesh";
  int age = 28;
  Student(String name , int age){
    this.name = name;
    this.age = age;
  }
  void displayInfo(){
    print('name:$name,Age:$age');
  }
}
void main(){
  var student1 = Student('Ganesh',20);
  print(student1);
  //student1.incrementAge();
  student1.age+=1;
  //student1.incrementAge(student1);
 // student1.age = 23;
  student1.displayInfo();
}