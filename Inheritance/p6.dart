class Demo {
  void fun(){
    print("In fun");
  }
  void gun(){
    print("In gun");
  }
}
mixin Test implements Demo{

  void fun(){
    print("In mixin");
  }

}
class Test2 with Test{
  void fun(){
    print(".....In fun");

  }
  void gun(){
    print("In fun");
  }
}
void main(){
  Test2 obj=new Test2();
  obj.fun();
}