int fun(int x){
  if(x == 0 ){
    return 1;
  }
  return fun(x-1) + fun(x-2);
}
void main(){
 print(fun(5));
}