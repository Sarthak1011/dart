int fun(int x){

  if(x == 0){
    return 1;
  }
  return x * fun(--x);
}
void main(){

  print(fun(5));

}