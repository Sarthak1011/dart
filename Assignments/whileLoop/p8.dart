/*
Program 8: Write a program to print the product of odd digits between 10
to 1
Output: 945
 */

void main(){

  int pro = 1;

  int num =1;
  while(num <=10){
    if(num %2 == 1)
    pro =pro*num;
    num ++;
  }
  print(pro);

}