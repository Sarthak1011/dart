/*
Program 5: Write a program to print the square of even digits between
40 to 50
Output: 1600 1764 1936 2116 2304 2500
*/

void main(){

  int num = 40;
  while(num<=50){
    print(num*num);
    num=num+2;
  }
}