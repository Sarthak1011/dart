/*
rogram 10: Write a program to calculate the factorial of the given
number.
num = 6;
Output: factorial 6 is 720
 */

void main(){

int fact = 1;
int num = 6;
while(num !=0){

  fact= fact * num ;
  num --;
}
print(fact);
}