/*
Q2
Write a program to print the following pattern
4 5 6 7
4 5 6 7
4 5 6 7
4 5 6 7
USE THIS FOR LOOP STRICTLY
for(int i =1;i<=4;i++){
}
*/
import "dart:io";
void main(){

  int row = 4;

  for(int i =1;i<=row;i++){
    int num = row;
    for(int j = 1;j<=row;j++){
      stdout.write('$num \t');
      num++;
    }
    print("");
  }
}