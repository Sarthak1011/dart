/*
Q4
Write a program to print the following pattern
1A 1A 1A
1A 1A 1A
1A 1A 1A
for(int i =1;i<=3;i++){
}

*/
import 'dart:io';
void main(){

int row = 4;
for(int i = 1;i<=row;i++){
  for(int j = 1;j<=row;j++){
    stdout.write('1A\t');
  }
  print("");
}
}