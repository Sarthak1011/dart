/*
Program 4 :
Write a dart program, take a number and print whether it is 
positive or negative. 
Input: var=5
Output: 5 is a positive number
Input: var=-9
Output: -9 is a negative number
*/

void main(){

  int num = 10;

  if(num == 0)
    print('you entered zero');
    else if(num <0)
      print('$num is a negative number');
    else 
      print('$num is positive number');

}