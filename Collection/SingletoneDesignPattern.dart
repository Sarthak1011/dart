class SingletoneDesignPattern{

  static SingletoneDesignPattern obj = new SingletoneDesignPattern._private();

  SingletoneDesignPattern._private(){
    print("In Private Constructor");
   // print(obj.hashCode);
  }
  factory SingletoneDesignPattern(){
    print("In Factory Constructor");
    //print(obj.hashCode);
    return obj;
  }
}
void main(){
  SingletoneDesignPattern obj1 = new SingletoneDesignPattern();
  SingletoneDesignPattern obj2 = new SingletoneDesignPattern();
  SingletoneDesignPattern obj3 = new SingletoneDesignPattern();

  print(obj1.hashCode);
  print(obj2.hashCode);
  print(obj3.hashCode);

}